-- create users table
CREATE TABLE users(

	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL, 
	fullname VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id)
);

--create 5 user records
INSERT INTO users(username, password, fullname, contact_number, email, address) VALUES ("john1", "pass1", "john doe", 0123456789, "john@mail.com", "boston"), ("hera2", "pass2", "hera doe", 0123456789, "hera@mail.com", "miami"), ("grace3", "pass3", "grace doe", 0123456789, "grace@mail.com", "boston"), ("jimmy4", "pass4", "jimmy doe", 0123456789, "jimmy@mail.com", "los angeles"), ("brook5", "pass5", "brook doe", 0123456789, "brook@mail.com", "memphis");

--create reviews table
CREATE TABLE reviews(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	review VARCHAR(500) NOT NULL,
	datetime_created DATETIME NOT NULL,
	rating INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_reviews_user_id 
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

--create 5 review records
INSERT INTO reviews(user_id, review, datetime_created, rating) VALUES (1, "The songs are okay. Worth the subscription", "2023-05-03", 5), (2, "The songs are meh. I want BLACKPINK", "2023-01-03", 1), (3, "Add bruno mars and lady gaga", "2023-03-23", 4), (4, "I want to listen to more k-pop", "2022-09-23", 3), (5, "Kindly add more OPM", "2023-02-01", 5) ;

--Filter the users with k in their name and join the reviews and users table

SELECT * FROM users JOIN reviews ON users.id = reviews.user_id WHERE fullname LIKE "%k%";

--Filter with x in their name

SELECT * FROM users JOIN reviews ON users.id = reviews.user_id WHERE fullname LIKE "%x%";

--Join the reviews and users table
SELECT * FROM users JOIN reviews ON users.id = reviews.user_id

--Get the review and username only when joining the reviews and users table

SELECT reviews.review, users.username FROM users JOIN reviews ON users.id = reviews.user_id;
